# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Lasse Liehu <lasse.liehu@gmail.com>, 2014, 2015.
# Tommi Nieminen <translator@legisign.org>, 2018, 2019, 2020, 2021, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-04-26 00:39+0000\n"
"PO-Revision-Date: 2023-05-29 21:17+0300\n"
"Last-Translator: Tommi Nieminen <translator@legisign.org>\n"
"Language-Team: Finnish <kde-i18n-doc@kde.org>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 23.04.1\n"

#: contents/ui/BarcodePage.qml:23
#, kde-format
msgid "QR Code"
msgstr "QR-koodi"

#: contents/ui/BarcodePage.qml:24
#, kde-format
msgid "Data Matrix"
msgstr "Data Matrix"

#: contents/ui/BarcodePage.qml:25
#, kde-format
msgctxt "Aztec barcode"
msgid "Aztec"
msgstr "Aztec"

#: contents/ui/BarcodePage.qml:26
#, kde-format
msgid "Code 39"
msgstr "Koodi 39"

#: contents/ui/BarcodePage.qml:27
#, kde-format
msgid "Code 93"
msgstr "Koodi 93"

#: contents/ui/BarcodePage.qml:28
#, kde-format
msgid "Code 128"
msgstr "Koodi 128"

#: contents/ui/BarcodePage.qml:44
#, kde-format
msgid "Return to Clipboard"
msgstr "Palaa leikepöydälle"

#: contents/ui/BarcodePage.qml:79
#, kde-format
msgid "Change the QR code type"
msgstr "Vaihda QR-koodin tyyppiä"

#: contents/ui/BarcodePage.qml:136
#, kde-format
msgid "Creating QR code failed"
msgstr "QR-koodin luonti epäonnistui"

#: contents/ui/BarcodePage.qml:148
#, kde-format
msgid ""
"There is not enough space to display the QR code. Try resizing this applet."
msgstr "Tila ei riitä QR-koodin näyttämiseksi. Yritä muuttaa sovelman kokoa."

#: contents/ui/DelegateToolButtons.qml:26
#, kde-format
msgid "Invoke action"
msgstr "Käytä toimintoa"

#: contents/ui/DelegateToolButtons.qml:41
#, kde-format
msgid "Show QR code"
msgstr "Näytä QR-koodi"

#: contents/ui/DelegateToolButtons.qml:57
#, kde-format
msgid "Edit contents"
msgstr "Muokkaa sisältöä"

#: contents/ui/DelegateToolButtons.qml:71
#, kde-format
msgid "Remove from history"
msgstr "Poista historiasta"

#: contents/ui/EditPage.qml:82
#, kde-format
msgctxt "@action:button"
msgid "Save"
msgstr "Tallenna"

#: contents/ui/EditPage.qml:90
#, kde-format
msgctxt "@action:button"
msgid "Cancel"
msgstr "Peru"

#: contents/ui/main.qml:30
#, kde-format
msgid "Clipboard Contents"
msgstr "Leikepöydän sisältö"

#: contents/ui/main.qml:31 contents/ui/Menu.qml:136
#, kde-format
msgid "Clipboard is empty"
msgstr "Leikepöytä on tyhjä"

#: contents/ui/main.qml:59
#, kde-format
msgid "Clear History"
msgstr "Tyhjennä historia"

#: contents/ui/main.qml:71
#, kde-format
msgid "Configure Clipboard…"
msgstr "Leikepöydän asetukset…"

#: contents/ui/Menu.qml:136
#, kde-format
msgid "No matches"
msgstr "Ei osumia"

#: contents/ui/UrlItemDelegate.qml:107
#, kde-format
msgctxt ""
"Indicator that there are more urls in the clipboard than previews shown"
msgid "+%1"
msgstr "+%1"

#~ msgid "The QR code is too large to be displayed"
#~ msgstr "QR-koodi on näytettäväksi liian suuri"

#~ msgid "Clear history"
#~ msgstr "Tyhjennä historia"

#~ msgid "Search…"
#~ msgstr "Etsi…"

#~ msgid "Clipboard history is empty."
#~ msgstr "Leikepöydän historia on tyhjä."

#~ msgid "Configure"
#~ msgstr "Asetukset"
