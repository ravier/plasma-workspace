# Copyright (C) 2024 This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
#
# SPDX-FileCopyrightText: 2021, 2023, 2024 Kishore G <kishore96@gmail.com>
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-03-19 00:40+0000\n"
"PO-Revision-Date: 2024-03-24 17:32+0530\n"
"Last-Translator: Kishore G <kishore96@gmail.com>\n"
"Language-Team: Tamil <kde-i18n-doc@kde.org>\n"
"Language: ta\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 24.02.1\n"

#: baloosearchrunner.cpp:60
#, kde-format
msgid "Open Containing Folder"
msgstr "கொண்டுள்ள அடைவை திற"

#: baloosearchrunner.cpp:84
#, kde-format
msgctxt ""
"Audio files; translate this as a plural noun in languages with a plural form "
"of the word"
msgid "Audio"
msgstr "ஒலிக்கோப்புகள்"

#: baloosearchrunner.cpp:86
#, kde-format
msgid "Images"
msgstr "படங்கள்"

#: baloosearchrunner.cpp:87
#, kde-format
msgid "Videos"
msgstr "ஒளிக்காட்சிகள்"

#: baloosearchrunner.cpp:88
#, kde-format
msgid "Spreadsheets"
msgstr "விரிதாள்கள்"

#: baloosearchrunner.cpp:89
#, kde-format
msgid "Presentations"
msgstr "வில்லைக்காட்சிகள்"

#: baloosearchrunner.cpp:90
#, kde-format
msgid "Folders"
msgstr "அடைவுகள்"

#: baloosearchrunner.cpp:91
#, kde-format
msgid "Documents"
msgstr "ஆவணங்கள்"

#: baloosearchrunner.cpp:92
#, kde-format
msgid "Archives"
msgstr "பெட்டகங்கள்"

# Assuming this means 'text files', and not 'text messages'
#: baloosearchrunner.cpp:93
#, kde-format
msgid "Texts"
msgstr "உரைக்கோப்புகள்"

#: baloosearchrunner.cpp:94
#, kde-format
msgid "Files"
msgstr "கோப்புகள்"
